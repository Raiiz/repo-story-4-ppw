from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from .views import register

# Create your tests here.
class Auth_Test(TestCase):
    def test_is_register_page_exists(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register/register.html')
        found = resolve('/register/')
        self.assertEqual(found.func, register)
    
    def test_dummy_register(self):
        response = Client().post('/register/', {'first_name': 'Muhammad', 'last_name': 'Placeholder', 'email' : 'placeholder@gmail.com', 'username':'holder', 'password1' : 'sampelpassword1', 'password2' : 'sampelpassword1'}, format='text/html')
        amount = User.objects.count()
        self.assertEqual(amount, 1)