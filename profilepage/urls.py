from django.urls import path
from . import views

app_name = 'profilepage'

urlpatterns = [
    path('', views.landing_page, name='landing_page'),
    path('my-profile/', views.profile_page, name='profile_page'),
    path('my-games/', views.game_page, name='game_page'),
    path('my-accordion/', views.accordion_page, name='accordion_page'),
    path('my-booksearcher/', views.booksearcher_page, name='booksearcher_page'),
    path('my-booksearcher/<str:title>/', views.booksearcher_page_api, name='booksearcher_page_api'),
    path('my-welcome/', views.welcome_page, name='welcome_page'),
    path('my-courses/', views.course_display_view, name='course_display_view'),
    path('my-courses/<int:my_id>/', views.course_detail_view, name='course_detail_view'),
    path('my-courses/<int:my_id>/delete/', views.course_delete_view, name='course_delete_view'),
    path('my-courses/create/', views.course_create_view, name='course_create_view'),
    path('my-activities/activity-create/', views.activity_create_view, name='activity_create_view'),
    path('my-activities/activity-registration/', views.activity_registration_view, name='activity_registration_view'),
    path('my-activities/', views.activity_display_view, name='activity_display_view'),
]