from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import Course, Activity, Person
from .views import landing_page, welcome_page, profile_page, game_page, accordion_page, booksearcher_page, booksearcher_page_api
from .views import course_create_view, course_delete_view, course_detail_view, course_display_view
from .views import activity_create_view, activity_display_view, activity_registration_view
import datetime

# Create your tests here.
class Story9_Test(TestCase):

    # Checking absolute url
    def test_model_course_get_absolute_url(self): # UnitTest for Story 5
        models.Course.objects.create(title="PPW", term="Odd Term 2020/2021", lecturer="Arawinda Dinakaramani", credits="3", place="2.2306", description="Self-explanatory.")
        course = models.Course.objects.get(id=1)
        self.assertEqual(course.get_absolute_url(), '/my-courses/1/')

    # Checking model __str__
    def test_check_model_name_course(self): # unitTest for Story 5
        models.Course.objects.create(title="PPW", term="Odd Term 2020/2021", lecturer="Arawinda Dinakaramani", credits="3", place="2.2306", description="Self-explanatory.")
        course = models.Course.objects.get()
        self.assertEqual(str(course), "PPW")
    
    def test_check_model_name_activity(self): # UnitTest for Story 6
        models.Activity.objects.create(name="Skirmish", date=datetime.date(2020, 1, 1))
        activity = models.Activity.objects.get(name="Skirmish", date=datetime.date(2020, 1, 1))
        self.assertEqual(str(activity), "Skirmish")
    
    def test_check_model_name_person(self): # UnitTest for Story 6
        models.Activity.objects.create(name="Skirmish", date=datetime.date(2020, 2, 2))
        idpk = models.Activity.objects.all()[0].id
        activity = models.Activity.objects.get(id=idpk)
        models.Person.objects.create(full_name="Celica", gender="Female", activity_registered=activity)
        person = models.Person.objects.get(activity_registered=activity)
        self.assertEqual(str(person), "Celica")

    # Checking database
    def test_check_model_create_course(self): # UnitTest for Story 5
        models.Course.objects.create(title="PPW", term="Odd Term 2020/2021", lecturer="Arawinda Dinakaramani", credits="3", place="2.2306", description="Self-explanatory.")
        models.Course.objects.create(title="SDA", term="Odd Term 2020/2021", lecturer="Dipta Tanaya", credits="4", place="2.2303", description="Self-explanatory.")
        amount = Course.objects.all().count()
        self.assertEqual(amount, 2)

    def test_check_model_create_activity(self): # UnitTest for Story 6
        models.Activity.objects.create(name="Attack on Rigel Castle", date=datetime.date(2020, 10, 10))
        amount = Activity.objects.all().count()
        self.assertEqual(amount, 1)
    
    def test_check_model_create_activity_then_person(self): # UnitTest for Story 6
        models.Activity.objects.create(name="Attack on Rigel Castle", date=datetime.date(2020, 10, 10))
        activity = models.Activity.objects.get(name="Attack on Rigel Castle", date=datetime.date(2020, 10, 10))
        models.Person.objects.create(full_name="Alm", gender="Male", activity_registered=activity)
        models.Person.objects.create(full_name="Tatiana", gender="Female", activity_registered=activity)
        models.Person.objects.create(full_name="Zeke", gender="Male", activity_registered=activity)
        amount = Person.objects.all().count()
        self.assertEqual(amount, 3)

    # Checking URL, Views, Template
    def test_is_course_create_page_exists(self): # UnitTest for Story 5
        response = Client().get('/my-courses/create/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "course/course-create.html")
        found = resolve('/my-courses/create/')
        self.assertEqual(found.func, course_create_view)
    
    def test_is_course_display_page_exists(self): # UnitTest for Story 5
        response = Client().get('/my-courses/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "course/course-display.html")
        found = resolve('/my-courses/')
        self.assertEqual(found.func, course_display_view)
    
    def test_is_course_detail_page_exists(self): # UnitTest for Story 5
        models.Course.objects.create(title="SDA", term="Odd Term 2020/2021", lecturer="Dipta Tanaya", credits="4", place="2.2303", description="Self-explanatory.")
        response = Client().get('/my-courses/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "course/course-detail.html")
        found = resolve('/my-courses/1/')
        self.assertEqual(found.func, course_detail_view)
    
    def test_is_course_delete_page_exists(self): # UnitTest for Story 5
        models.Course.objects.create(title="SDA", term="Odd Term 2020/2021", lecturer="Dipta Tanaya", credits="4", place="2.2303", description="Self-explanatory.")
        response = Client().get('/my-courses/1/delete/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "course/course-delete.html")
        found = resolve('/my-courses/1/delete/')
        self.assertEqual(found.func, course_delete_view)

    def test_is_activity_create_page_exists(self): # UnitTest for Story 6
        response = Client().get('/my-activities/activity-create/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "activity/activity-create.html")
        found = resolve('/my-activities/activity-create/')
        self.assertEqual(found.func, activity_create_view)

    def test_is_activity_display_page_exists(self): # UnitTest for Story 6
        response = Client().get('/my-activities/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "activity/activity-display.html")
        found = resolve('/my-activities/')
        self.assertEqual(found.func, activity_display_view)

    def test_is_activity_registration_page_exists(self): # UnitTest for Story 6
        response = Client().get('/my-activities/activity-registration/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "activity/activity-registration.html")
        found = resolve('/my-activities/activity-registration/')
        self.assertEqual(found.func, activity_registration_view)

    def test_is_landing_page_exists(self): # UnitTest for Story 9
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'landing-page.html')
        found = resolve('/')
        self.assertEqual(found.func, landing_page)

    def test_is_welcome_page_exists(self): # UnitTest for Story 9
        response = Client().get('/my-welcome/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'welcome-page.html')
        found = resolve('/my-welcome/')
        self.assertEqual(found.func, welcome_page)
    
    def test_is_profile_page_exists(self): # UnitTest for Story 4
        response = Client().get('/my-profile/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile-page.html')
        found = resolve('/my-profile/')
        self.assertEqual(found.func, profile_page)
    
    def test_is_game_page_exists(self): # UnitTest for Story 4
        response = Client().get('/my-games/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'game-page.html')
        found = resolve('/my-games/')
        self.assertEqual(found.func, game_page)
    
    def test_is_accordion_page_exists(self): # UnitTest for Story 7
        response = Client().get('/my-accordion/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accordion/accordion-page.html')
        found = resolve('/my-accordion/')
        self.assertEqual(found.func, accordion_page)
    
    def test_is_booksearcher_page_exists(self): # UnitTest for Story 8
        response = Client().get('/my-booksearcher/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'booksearcher/booksearcher-page.html')
        found = resolve('/my-booksearcher/')
        self.assertEqual(found.func, booksearcher_page)

    def test_is_booksearcher_api_exists(self): # UnitTest for Story 8
        response = Client().get('/my-booksearcher/Persona%203/')
        self.assertEqual(response.status_code, 200)
        found = resolve('/my-booksearcher/Persona%203/')
        self.assertEqual(found.func, booksearcher_page_api)
    
    # Checking POST request
    def test_saving_a_POST_request_activity(self): # UnitTest for Story 6
        response = Client().post('/my-activities/activity-create/', data={'name': 'Liberation of Zofia Castle', 'date': '2020-12-12'})
        amount = models.Activity.objects.filter(name='Liberation of Zofia Castle').count()
        self.assertEqual(amount, 1)
    
    def test_saving_a_POST_request_person(self): # UnitTest for Story 6
        models.Activity.objects.create(name="Berkut Last Stand", date=datetime.date(2020, 10, 10))
        activity1 = Activity.objects.get(pk=1).pk
        response = Client().post('/my-activities/activity-registration/', data={'full_name': 'Delthea', 'gender': 'Female', 'activity_registered': activity1})
        amount = models.Person.objects.filter(full_name='Delthea').count()
        self.assertEqual(amount, 1)
    
    def test_saving_a_POST_request_course(self): # UnitTest for Story 5
        response = Client().post('/my-courses/create/', data={'title': 'DDAK', 'term': 'Odd Term 2020/2021', 'lecturer': "Bayu Anggorojati", 'credits': "4", 'place': "2.2304", 'description': "Self-explanatory."})
        amount = models.Course.objects.filter(title='DDAK').count()
        self.assertEqual(amount, 1)

    def test_saving_a_POST_request_delete(self): # UnitTest for Story 5
        models.Course.objects.create(title="DDAK", term="Odd Term 2020/2021", lecturer="Bayu Anggorojati", credits="4", place="2.2304", description="Self-explanatory.")
        response = Client().post('/my-courses/1/delete/', data={'title': 'DDAK', 'term': 'Odd Term 2020/2021', 'lecturer': "Bayu Anggorojati", 'credits': "4", 'place': "2.2304", 'description': "Self-explanatory."})
        amount = models.Course.objects.filter(title='DDAK').count()
        self.assertEqual(amount, 0)