from django import forms
from .models import Course, Activity, Person
from django.forms import ModelForm, Textarea, TextInput

NUMBERS = (('', 'Choose...'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), )
TERMS = (('', 'Choose...'), ('Odd Term 2020/2021', 'Odd Term 2020/2021'), ('Even Term 2019/2020', 'Even Term 2020/2021'), ('Odd Term 2019/2020', 'Odd Term 2019/2020'), )
GENDER = (('', 'Choose...'), ('Male', 'Male'), ('Female', 'Female'), ('Prefer not to answer', 'Prefer not to answer'), )

class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = "__all__"
        widgets = {
            'title': TextInput(attrs={'placeholder': 'ex: Computer & Society', 'required': True}),
            'term': forms.Select(choices=TERMS, attrs={'class': 'form-control', 'required': True}),
            'lecturer': TextInput(attrs={'placeholder': 'ex: Laksmita Rahadianti', 'required': True}),
            'credits': forms.Select(choices=NUMBERS, attrs={'class': 'form-control', 'required': True}),
            'place': TextInput(attrs={'placeholder': 'ex: 2.2502', 'required': True}),
            'description': Textarea(attrs={'rows': 5, 'cols': 50, 'placeholder': 'ex: This course is awesome.', 'required': True}),
            }

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={'placeholder': 'ex: Skirmish', 'required': True}),
            'date': TextInput(attrs={'placeholder': 'ex: 2000-10-20', 'required': True}),
        }

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = "__all__"
        widgets = {
            'full_name': TextInput(attrs={'placeholder': 'ex: Jonathan Joestar', 'required': True}),
            'gender': forms.Select(choices=GENDER, attrs={'class': 'form-control', 'required': True}),
        }