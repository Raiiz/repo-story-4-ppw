$("#search").keyup(function() {
    var search_query = $("#search").val();
    console.log(search_query);
    $.ajax({
        url: "/my-booksearcher/" + search_query,
        success: function(result) {
            var obj_result = $("#searchresults");
            obj_result.empty();
            var init_table = "";
            init_table += '<table class="table">';
            init_table += '<thead>';
            init_table += '<tr>';
            init_table += '<th>Thumbnail</th>';
            init_table += '<th>Title</th>';
            init_table += '<th>Author</th>';
            init_table += '<th>Description</th>';
            init_table += '<th>Visit Book</th>';
            init_table += '</tr>';
            init_table += '</thead>';
            init_table += '<tbody>';
            obj_result.append(init_table);
            if (result.items.length > 0) {
                console.log(result.items.length);
                for (var i = 0; i < result.items.length; i++) {
                    var tmp_author = result.items[i].volumeInfo.authors;
                    var tmp_desc = result.items[i].volumeInfo.description;
                    var shortener = jQuery.trim(tmp_desc).substring(0, 250).trim(this) + "...";
                    console.log(i);
                    console.log(result.items[i].volumeInfo.title);
                    var html = "";
                    html += '<tr>';
                    if (typeof result.items[i].volumeInfo.imageLinks !== 'undefined') {
                        html += '<td><img src=' + result.items[i].volumeInfo.imageLinks.smallThumbnail + ' alt="Book Cover"></td>';
                    } else {
                        html += '<td><img src="https://blkbekasi.kemnaker.go.id/subbagiankeuangan/assets-back-end/dist/img/instruksi/no-image-available.jpeg" alt="No Cover"></td>';
                    }
                    html += '<td>' + result.items[i].volumeInfo.title + '</td>';
                    if (typeof tmp_author === 'undefined') {
                        html += '<td>(Information not available)</td>';
                    } else {
                        html += '<td>' + tmp_author + '</td>';
                    }
                    if (typeof tmp_desc === 'undefined') {
                        html += '<td>(Information not available)</td>';
                    } else {
                        html += '<td>' + shortener + '</td>';
                    }
                    html += '<td><a href=' + result.items[i].volumeInfo.previewLink + ' target="_blank">Visit</a></td>';
                    obj_result.append(html);
                }
                var after_table = "";
                after_table += '</tbody>';
                after_table += '</table>';
                obj_result.append(after_table);
            }
        }
    })
})