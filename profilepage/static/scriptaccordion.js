$(".expand").click(function() {
    var select_1 = $(this).parent().parent();
    var select_2 = $(this).parents(".label");
    if (!select_1.hasClass("active")) {
        $(".accordion").removeClass("active");
        $(".label").find(".change").attr("src", "/static/img/plus.png");
        select_1.addClass("active");
        select_2.find(".change").attr("src", "/static/img/minus.png");
    } else if (select_1.hasClass("active")) {
        select_1.removeClass("active");
        select_2.find(".change").attr("src", "/static/img/plus.png");
    }
})

$(".move-up").click(function() {
    var select_content = $(this).parent().parent();
    select_content.prev().insertAfter(select_content);
})

$(".move-down").click(function() {
    var select_content = $(this).parent().parent();
    select_content.next().insertBefore(select_content);
})