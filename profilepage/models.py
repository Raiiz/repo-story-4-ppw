from django.db import models
from django.urls import reverse

NUMBERS = (('', 'Choose...'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), )
TERMS = (('', 'Choose...'), ('Odd Term 2020/2021', 'Odd Term 2020/2021'), ('Even Term 2019/2020', 'Even Term 2020/2021'), ('Odd Term 2019/2020', 'Odd Term 2019/2020'), )
GENDER = (('', 'Choose...'), ('Male', 'Male'), ('Female', 'Female'), ('Prefer not to answer', 'Prefer not to answer'), )

class Course(models.Model):
    title = models.CharField(max_length=160)
    term = models.CharField(max_length=160, choices=TERMS)
    lecturer = models.CharField(max_length=160)
    credits = models.CharField(max_length=1, choices=NUMBERS)
    place = models.CharField(max_length=99)
    description = models.CharField(max_length=500, default='')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f"/my-courses/{self.id}/"

class Activity(models.Model):
    name = models.CharField(max_length=160)
    date = models.DateField()

    def __str__(self):
        return self.name

class Person(models.Model):
    full_name = models.CharField(max_length=160)
    gender = models.CharField(max_length=20, choices=GENDER)
    activity_registered = models.ForeignKey(Activity, on_delete=models.CASCADE)

    def __str__(self):
        return self.full_name
