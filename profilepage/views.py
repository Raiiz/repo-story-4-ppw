from django.http import Http404, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from .models import Course, Activity, Person
from .forms import CourseForm, ActivityForm, PersonForm
import urllib.request, json

def landing_page(request): # Story 9
    return render(request, "landing-page.html")

def welcome_page(request): # Story 9
    num_visits = request.session.get('num_visits', 1)
    request.session['num_visits'] = num_visits + 1
    return render(request, "welcome-page.html", {'visit_amount': num_visits})

def profile_page(request): # Story 4
    return render(request, "profile-page.html")

def game_page(request): # Story 4
    return render(request, "game-page.html")

def accordion_page(request): # Story 7
    return render(request, "accordion/accordion-page.html")

def booksearcher_page(request): # Story 8
    return render(request, "booksearcher/booksearcher-page.html")

def booksearcher_page_api(request, title): # Story 8
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q=" + title,"/:?=")
    with urllib.request.urlopen(link) as url:
        global data
        data = json.loads(url.read().decode())
    return JsonResponse(data)

def course_display_view(request): # Story 5
    queryset = Course.objects.all()
    return render(request, "course/course-display.html", {'object_list': queryset})

def course_create_view(request): # Story 5
    my_form = CourseForm(request.POST or None)
    if my_form.is_valid():
        Course.objects.create(**my_form.cleaned_data)
        return redirect('/my-courses/')
    return render(request, "course/course-create.html", {'form': my_form})

def course_detail_view(request, my_id): # Story 5
    obj = get_object_or_404(Course, id=my_id)
    return render(request, "course/course-detail.html", {'object': obj})

def course_delete_view(request, my_id): # Story 5
    obj = get_object_or_404(Course, id=my_id)
    if request.method == 'POST':
        obj.delete()
        return redirect('/my-courses/')
    return render(request, "course/course-delete.html", {'object': obj})

def activity_create_view(request): # Story 6
    this_form = ActivityForm(request.POST or None)
    if this_form.is_valid():
        Activity.objects.create(**this_form.cleaned_data)
        return redirect('/my-activities/')
    context = {
        'page_title': 'List an activity',
        'form': this_form
    }
    return render(request, "activity/activity-create.html", context)

def activity_registration_view(request): # Story 6
    the_form = PersonForm(request.POST or None)
    if the_form.is_valid():
        Person.objects.create(**the_form.cleaned_data)
        return redirect('/my-activities/')
    context = {
        'page_title': 'Register to an activity',
        'form': the_form
    }
    return render(request, "activity/activity-registration.html", context)

def activity_display_view(request): # Story 6
    person_entries = Person.objects.all()
    activity_entries = Activity.objects.all()
    context = {
        'page_title': 'Display activity',
        'person': person_entries,
        'activity': activity_entries
    }
    return render(request, "activity/activity-display.html", context)