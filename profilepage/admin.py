from django.contrib import admin
from.models import Course, Activity, Person

# Register your models here.
admin.site.register(Course)
admin.site.register(Activity)
admin.site.register(Person)